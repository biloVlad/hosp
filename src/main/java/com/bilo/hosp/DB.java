package com.bilo.hosp;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DB {

    private static final Logger LOG = LoggerFactory.getLogger(Hosp.class.getName());
    private static DB instance = null;
    private static MongoDatabase database = null;

    private DB() {
        String dbProp = System.getProperty("database");
        LOG.warn("Connecting to MongoDB: {}", dbProp);
        MongoClient mongoClient = new MongoClient();
        
        try {
            database = mongoClient.getDatabase(dbProp);
        } catch (Exception e) {
            LOG.error("Get DB error: {}", e.getMessage());
        }

        LOG.info("Database created: {}", dbProp);
    }

    public static DB getInstance() {
        DB localInstance = instance;
        if (localInstance == null) {
            synchronized (Config.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new DB();
                }
            }
        }
        return localInstance;
    }

    public MongoDatabase getDatabase() {
        return database;
    }
}
