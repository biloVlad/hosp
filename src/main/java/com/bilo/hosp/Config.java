package com.bilo.hosp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config {

    private static final Logger LOG = LoggerFactory.getLogger(Config.class.getName());
    private static Config instance = null;

    private Config() {

        LOG.info("Conf: load: acsk.properties = {}", System.getProperty("path.to.config"));
        InputStream file = null;
        
        try {
            file = new FileInputStream(new File(System.getProperty("path.to.config")));
        } catch (FileNotFoundException e) {
            LOG.error("File config not found. Error: {}", e.getMessage());
        }

        Properties props = new Properties();
        try {
            props.load(file);
            for (String key : props.stringPropertyNames()) {
                String value = props.getProperty(key);

                LOG.warn("SYSPROPS: {} = {}", key, value);
                System.setProperty(key, value);
            }
        } catch (Exception e) {
            LOG.error("ERROR: {}", e.getMessage());
        }
    }

    public static Config getInstance() {
        Config localInstance = instance;
        if (localInstance == null) {
            synchronized (Config.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Config();
                }
            }
        }
        return localInstance;
    }
}
