package com.bilo.hosp.controller;

import com.bilo.hosp.DB;
import com.bilo.hosp.service.SetTemperatureService;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.IOUtils;
import org.bson.Document;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SetTemperature extends HttpHandler {

    private static final Logger LOG = LoggerFactory.getLogger(SetTemperature.class.getName());

    public SetTemperature() {

    }

    @Override
    public void service(Request rqst, Response rspns) throws Exception {
        rspns.setHeader("Content-Type", "text/plain");
        rspns.setCharacterEncoding("utf-8");

        LOG.info("Get DB instance");
        DB dbLink = DB.getInstance();

        String tParam = rqst.getParameter("t");
        
       
        SetTemperatureService service = new SetTemperatureService();
        String result = service.task(tParam, dbLink);

       try {
            rspns.getWriter().write(result);
            rspns.flush();
        } catch (Exception ex) {
            LOG.error("ERROR {}", ex);

            rqst.setAttribute("errorMessage", ex.getMessage());
            rqst.setAttribute("codeError", 500);

            rspns.sendError(500, "Error writing result");
        } 
    }
}
