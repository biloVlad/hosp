package com.bilo.hosp.controller;
import com.bilo.hosp.service.CheckTemperatureService;
import com.bilo.hosp.DB;
import com.bilo.hosp.service.SetRoomService;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckTemperature extends HttpHandler {
     private static final Logger LOG = LoggerFactory.getLogger(CheckTemperature.class.getName());
     
      @Override
    public void service(Request rqst, Response rspns) throws Exception {
        rspns.setHeader("Content-Type", "application/json");
        rspns.setCharacterEncoding("utf-8");      

        LOG.info("Get DB instance");
        DB dbLink = DB.getInstance();

        CheckTemperatureService service = new CheckTemperatureService();
        String result = service.task(dbLink);

        try {
            rspns.getWriter().write(result);
            rspns.flush();
        } catch (Exception ex) {
            LOG.error("ERROR {}", ex);

            rqst.setAttribute("errorMessage", ex.getMessage());
            rqst.setAttribute("codeError", 500);

            rspns.sendError(500, "Error writing result");
        } 
    }
}
