package com.bilo.hosp.controller;

import com.bilo.hosp.DB;
import com.bilo.hosp.service.NewPatientService;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NewPatient extends HttpHandler {

    private static final Logger LOG = LoggerFactory.getLogger(NewPatient.class.getName());

    public NewPatient() {

    }

    @Override
    public void service(Request rqst, Response rspns) throws Exception {
        rspns.setHeader("Content-Type", "application/json");
        rqst.setCharacterEncoding("utf-8");
        rspns.setCharacterEncoding("utf-8");

        LOG.info("Get DB instance");
        DB dbLink = DB.getInstance();

        String buff = null;
        try {
            buff = IOUtils.toString(rqst.getReader());
        } catch (IOException ex) {
            LOG.error("ERROR {}", ex);

            rqst.setAttribute("errorMessage", ex.getMessage());
            rqst.setAttribute("errorCode", 400);

            rspns.sendError(400, "Error getting input stream");

            return;
        }
        if ((buff == null) || (buff.length() == 0)) {
            LOG.error("ERROR", "No request bytes");

            rqst.setAttribute("errorMessage", "No request bytes");
            rqst.setAttribute("errorCode", 400);

            rspns.sendError(400, "No request bytes.");

            return;
        }
        
        NewPatientService service = new NewPatientService();
        String result = service.task(buff, dbLink);

        try {
            rspns.getWriter().write(result);
            rspns.flush();
        } catch (Exception ex) {
            LOG.error("ERROR {}", ex);

            rqst.setAttribute("errorMessage", ex.getMessage());
            rqst.setAttribute("codeError", 500);

            rspns.sendError(500, "Error writing result");
        }
    }

}
