package com.bilo.hosp;

import com.bilo.hosp.controller.*;
import java.io.IOException;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.ServerConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Hosp {

    private static final Logger LOG = LoggerFactory.getLogger(Hosp.class.getName());

    public static void main(String[] args) throws Exception {
        //Загрузка конфига
        LOG.info("First init Config");
        Config conf = Config.getInstance();

        //Получение link`a базы данных
        LOG.info("First init DB");
        DB dbLink = DB.getInstance();

        // Запуск HTTP сервера   
        HttpServer server = HttpServer.createSimpleServer("/hosp", "localhost", 8080);
        final ServerConfiguration config = server.getServerConfiguration();

        // Подключение контроллеров
        config.addHttpHandler(new GetPatients(), "/hosp/patient");        
        config.addHttpHandler(new DeletePatients(), "/hosp/patient/delete");
        config.addHttpHandler(new CheckTemperature(), "/hosp/check/temperature");        
        config.addHttpHandler(new NewPatient(), "/hosp/patient/new"); // Добавление нового пациента
        config.addHttpHandler(new LinkTemperature(), "/hosp/patient/link/temperature"); // Присвоить измеряемую температуру
        config.addHttpHandler(new SetTemperature(), "/hosp/patient/set/temperature"); // Задать измеряемую температуру
        config.addHttpHandler(new SetRoom(), "/hosp/patient/set/room"); // Задать палату пациента
        config.addHttpHandler(new GetTemperatures(), "/hosp/patient/get/temperatures");


        config.setJmxEnabled(true);
        try {
            server.start();
            Thread.currentThread().join();
        } catch (IOException e) {
            LOG.error("ERROR", e);
        } finally {
            server.shutdownNow();
        }
    }

}
