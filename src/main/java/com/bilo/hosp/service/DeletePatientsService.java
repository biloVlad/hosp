package com.bilo.hosp.service;

import com.bilo.hosp.DB;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeletePatientsService {

    private static final Logger LOG = LoggerFactory.getLogger(DeletePatientsService.class.getName());

    public String task(String buff, DB linkDB) {
        Document result = new Document();
        String collectionName = System.getProperty("collectionForAddPatient");
        Document doc = Document.parse(buff);

        Document personDoc = (Document) doc.get("document");

        // Получение коллекции для работы с БД
        MongoCollection<Document> collection = linkDB.getDatabase().getCollection(collectionName);
        LOG.info("Set collection database: " + collectionName);

        String pnum = personDoc.getString("pnum");
        String pser = personDoc.getString("pser");

        Document target = collection.find(new Document("document",
                new Document("pnum", pnum)
                        .append("pser", pser)))
                .first();
        if(collection.deleteOne(and(eq("document.pnum", pnum), eq("document.pser", pser))).getDeletedCount() > 0) {
             result.append("message", "Дані про пацієнта видалено")
                .append("status", "success");
        } else {
            result.append("message", "Пацієнта з такими даними не знайдено")
                .append("status", "error");
        }

       

        LOG.info(result.toJson());
        return result.toJson();
    }
}
