package com.bilo.hosp.service;

import com.bilo.hosp.DB;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NewPatientService {

    private static final Logger LOG = LoggerFactory.getLogger(NewPatientService.class.getName());

    public NewPatientService() {

    }

    public String task(String buff, DB linkDB) {
        Document result = new Document();
        String collectionName = System.getProperty("collectionForAddPatient");
        Document doc = Document.parse(buff);

        Document personDoc = (Document) doc.get("document");

        // Получение коллекции для работы с БД
        MongoCollection<Document> collection = linkDB.getDatabase().getCollection(collectionName);
        LOG.info("Set collection database: " + collectionName);
       
        String pnum = personDoc.getString("pnum");
        String pser = personDoc.getString("pser");    

        if (collection.find(new Document("document",
                new Document("pnum", pnum)
                        .append("pser", pser)))
                .first() != null) {
            result.append("message", "Пацієнт з наданими паспортними даними вже існує")
                    .append("status", "error");

            LOG.info(result.toJson());
            return result.toJson();
        }

        try {
            LOG.info("Document is generated: {}", doc);

            collection.insertOne(doc);

            result.append("message", "Пацієнт успішно зареєстрований!");
            result.append("status", "success");
            LOG.info(result.toJson());
        } catch (MongoException ex) {
            LOG.error("ERROR {}", ex);
        }
        return result.toJson();
    }
}
