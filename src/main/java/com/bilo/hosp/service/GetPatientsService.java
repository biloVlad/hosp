package com.bilo.hosp.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import com.bilo.hosp.DB;
import com.mongodb.Block;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetPatientsService {

    private static final Logger LOG = LoggerFactory.getLogger(GetPatientsService.class.getName());

    public GetPatientsService() {

    }

    public String task(DB linkDB) throws UnsupportedEncodingException {

        String collForAdd = System.getProperty("collectionForAddPatient");        

        MongoCollection<Document> collection = linkDB.getDatabase().getCollection(collForAdd);
        List<String> result = new ArrayList<>();

        collection.find().forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                result.add(document.toJson());
            }
        });

        LOG.info("Get all patients");       

        return result.toString();
    }
}
