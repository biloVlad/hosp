package com.bilo.hosp.service;

import com.bilo.hosp.DB;
import com.bilo.hosp.controller.SetRoom;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import com.mongodb.client.model.Updates;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LinkTemperatureService {

    private static final Logger LOG = LoggerFactory.getLogger(LinkTemperatureService.class.getName());

    public LinkTemperatureService() {

    }

    public String task(String buff, DB linkDB) throws IOException {
        Document result = new Document();
        String collForAdd = System.getProperty("collectionForAddPatient");
        MongoCollection<Document> collection = linkDB.getDatabase().getCollection(collForAdd);
        Document doc = Document.parse(buff);

        Document personDoc = (Document) doc.get("document");
        String pnum = personDoc.getString("pnum");
        String pser = personDoc.getString("pser");

        Document data = (Document) doc.get("data");

        String t = data.getString("t");
        if (t == null || t.isEmpty()) {
            result.append("status", "error");
            result.append("message", "Відсутній показник температури");
            return result.toJson();
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        Document tDoc = new Document().append("t", t)
                .append("date", dateFormat.format(date));

        Document targetPatient = collection.findOneAndUpdate(
                and(eq("document.pnum", pnum), eq("document.pser", pser)),
                Updates.addToSet("info.data.temperatures", tDoc)
        );

        System.out.println(targetPatient);
        if (targetPatient != null) {
            result.append("status", "success");
            result.append("message", "Температуру пацієнта '" + targetPatient.getString("name") + " " + targetPatient.getString("lastname") + "' змінено на " + t);
        } else {
            result.append("status", "error");
            result.append("message", "Пацієнта не знайдено");
        }

        LOG.info(result.toJson());

        return result.toJson();
    }
}
