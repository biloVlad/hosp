package com.bilo.hosp.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import com.bilo.hosp.DB;
import com.mongodb.Block;
import com.mongodb.client.MongoCollection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckTemperatureService {

    private static final Logger LOG = LoggerFactory.getLogger(CheckTemperatureService.class.getName());

    public CheckTemperatureService() {

    }

    public String task(DB linkDB) throws UnsupportedEncodingException {
        Document result = new Document();
        String collForAdd = System.getProperty("collectionForMeasurementingTemp");

        MongoCollection<Document> collection = linkDB.getDatabase().getCollection(collForAdd);

        Document temp = collection.find().first();
        Date dateNow = new Date();
        Date dateTemp = new Date(temp.getString("date"));

        if (dateNow.getTime() - dateTemp.getTime() > 5000) {
            result.append("status", "time-out");
        } else {
            result.append("status", "success");
            result.append("value", temp.getString("t"));
        }

        return result.toJson();
    }
}
