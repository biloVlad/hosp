package com.bilo.hosp.service;

import com.bilo.hosp.DB;
import com.bilo.hosp.controller.SetRoom;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.set;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SetRoomService {

    private static final Logger LOG = LoggerFactory.getLogger(SetRoomService.class.getName());

    public SetRoomService() {

    }

    public String task(String buff, DB linkDB) {
        Document result = new Document();
        String collForAdd = System.getProperty("collectionForAddPatient");
        Document doc = Document.parse(buff);

        Document personDoc = (Document) doc.get("document");

        String pnum = personDoc.getString("pnum");
        String pser = personDoc.getString("pser");         

        String room = doc.getString("room");

        LOG.warn(pnum + " - " + pser + " - " + room);

        MongoCollection<Document> collection = linkDB.getDatabase().getCollection(collForAdd);

        Document targetPatient = collection.findOneAndUpdate(
                and(eq("document.pnum", pnum), eq("document.pser", pser)),
                set("room", room));

        if (targetPatient != null) {
            result.append("message", "Палату пацієнта '" + targetPatient.getString("name") + " " + targetPatient.getString("lastname") + "' змінено на " + room);
            result.append("status", "success");
        } else {
            result.append("status", "error");
            result.append("message", "Пацієнта не знайдено");
        }

        LOG.info(result.toJson());

        return result.toJson();
    }
}
