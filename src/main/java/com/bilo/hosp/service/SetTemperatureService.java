package com.bilo.hosp.service;

import com.bilo.hosp.DB;
import com.bilo.hosp.controller.SetRoom;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import com.mongodb.client.model.Updates;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SetTemperatureService {

    private static final Logger LOG = LoggerFactory.getLogger(SetTemperatureService.class.getName());

    public SetTemperatureService() {

    }

    public String task(String buff, DB linkDB) throws IOException {
        Document result = new Document();
        String collForTemp = System.getProperty("collectionForMeasurementingTemp");
        MongoCollection<Document> collection = linkDB.getDatabase().getCollection(collForTemp);
        if (buff != null) {
            String tParam = buff;

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();

            collection.drop();

            LOG.info("First set");
            Document tDoc = new Document().append("t", tParam)
                    .append("date", dateFormat.format(date));
            collection.insertOne(tDoc);

            result.append("status", "success");
            result.append("message", "Отримано температуру: " + tParam);

        } else {
            result.append("status", "error");
            result.append("message", "Показник відсутній");
        }

        LOG.info(result.toJson());

        return result.toJson();
    }
}
