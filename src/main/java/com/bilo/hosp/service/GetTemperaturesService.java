package com.bilo.hosp.service;

import com.bilo.hosp.DB;
import com.mongodb.Block;
import com.mongodb.client.MongoCollection;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetTemperaturesService {

    private static final Logger LOG = LoggerFactory.getLogger(GetTemperaturesService.class.getName());
    
    public GetTemperaturesService() {
        
    }
    
    public String task(DB linkDB) throws UnsupportedEncodingException {
        
        String collForAdd = System.getProperty("collectionForAddPatient");        
        
        MongoCollection<Document> collection = linkDB.getDatabase().getCollection(collForAdd);
        List<String> result = new ArrayList<>();
        
        collection.find().forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                Document person = new Document();
                Document info = (Document) document.get("info");                
                Document data = (Document) info.get("data");       
                Document docPers = (Document) document.get("document");
                
                person.append("name", document.getString("name"))
                        .append("surename", document.getString("surename"))
                        .append("lastname", document.getString("lastname"))
                        .append("room", document.getString("room"))
                        .append("document", docPers)
                        .append("info", data);
               
                result.add(person.toJson());
            }
        });
        
        LOG.info("Get all patients with temperatures only");        
        
        return result.toString();
    }
}
