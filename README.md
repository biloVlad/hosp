**json для створення нового пацієнта:**
```json
{
    "name":"Влад",
    "surename":"Васильевич",
    "lastname":"Бильо",
    "org":"Поликлиника",
    "orgunit":"5",
    "room":"13b",
    "document":{
        "pnum":"846758",
        "pser":"EA"
    },
    "info":{
       "data":{
           "temperatures":[
           ]
       },
        "medication":{
            "doctor":"Корзин Олег Николаевич",
            "name":"Мукалтин",
            "cod":"746df",
            "dosage":{
                "time":"2",
                "count":"1"
            }
        }
    }
}
```


**json для встановлення температури:**
```json
{
  "document": {
    "pnum": "816478",
    "pser": "EA"
  },
  "data": {
    "t": "36.6"
  }
}
```

**json for relocate:**
```json
{
  "document": {
    "pnum": "816478",
    "pser": "EA"
  },
  "room": "13b"
}
```

*Можливі відповіді від сервара:*

  *- дані вже існують*
```json
{
  "message": "Person with same documents already exists",
  "status": "error"
}
```

  *- дані успішно додано*
```json
  {
  "message": "Success insert",
  "status": "success"
}
```

  *- температуру встановлено*
```json
  {
  "status": "success",
  "message": "Температуру пацієнта '__' змінено на 36.6"
}
```

  *- пацієнта не знайдено*
```json
  {
  "status": "error",
  "message": "Пацієнта не знайдено"
}
```

  *- палату пацієнта змінено*
```json
  {
  "message": "Палату пацієнта '__' змінено на 13b",
  "status": "success"
}
```

